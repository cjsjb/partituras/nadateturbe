\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

%		\clef "soprano"
		\clef "treble"

		\transpose c' bes {
			\key c \major
		R1*4  |
%% 5
		r2 e' 8 d' c' d' ~  |
		d' 8 g' 2. r8  |
		r4 r f' 8. g' a' 8  |
		g' 4 d' 2 r4  |
		r2 e' 8 d' c' d' ~  |
%% 10
		d' 8 g' 2. r8  |
		r2 f' 8. g' a' 8  |
		a' 4 g' 2 r4  |
		r2 a' 4 c''  |
		b' 4. g' 2 r8  |
%% 15
		r2 a' 8 g' f' f' ~  |
		f' 8 e' 2 r8 r4  |
		r2 gis' 8. a' b' 8  |
		b' 4 a' 2 r4  |
		r2 c'' 8 c'' c'' c'' ~  |
%% 20
		c'' 8 b' 2 r8 r4  |
		r2 \times 2/3 { g' 4 a' b' }  |
		c'' 2 g'  |
		r2 \times 2/3 { d'' 4 c'' b' }  |
		c'' 2 a'  |
%% 25
		r2 \times 2/3 { g' 4 a' b' }  |
		c'' 2 a'  |
		r2 \times 2/3 { c'' 4 c'' c'' }  |
		b' 1  |
		r2 \times 2/3 { g' 4 a' b' }  |
%% 30
		c'' 2 g'  |
		r2 \times 2/3 { d'' 4 c'' b' }  |
		c'' 2 a'  |
		r2 \times 2/3 { g' 4 a' b' }  |
		c'' 2 a'  |
%% 35
		r2 \times 2/3 { c'' 4 c'' c'' }  |
		b' 1  |
		r2 \times 2/3 { g' 4 g' g' }  |
		g' 1  |
		}
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		Na -- da te tur __ be,
		na -- da "te es" -- pan -- te,
		to -- do se pa __ sa,
		Dios no se mu -- da.

		La pa -- cien -- cia
		to -- do "lo al" -- can __ za.
		Quien a Dios tie -- ne
		na -- da le fal __ ta.

		¡Só -- lo Dios bas -- ta!
		¡Só -- lo Dios bas -- ta!
		¡Só -- lo Dios bas -- ta!
		¡A -- le -- lu -- ya!

		¡Só -- lo Dios bas -- ta!
		¡Só -- lo Dios bas -- ta!
		¡Só -- lo Dios bas -- ta!
		¡A -- le -- lu -- ya!

		¡A -- le -- lu -- ya!
	}
>>
