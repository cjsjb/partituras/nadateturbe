\context ChordNames
	\chords {
		\set chordChanges = ##t
		\set chordNameFunction = #parenthesis-ignatzek-chord-names
		\set instrumentName = #"(En re)"
		\acordesendo
	}
