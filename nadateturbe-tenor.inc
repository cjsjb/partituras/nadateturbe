\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

%		\clef "tenor"
		\clef "treble_8"

		\transpose c' bes {
			\key c \major
		R1*20  |
		r2 \times 2/3 { g 4 a g }  |
		g 2 e  |
		r2 \times 2/3 { d 4 e f }  |
		f 2 c  |
%% 25
		r2 \times 2/3 { g 4 a g }  |
		a 2 e (  |
		f ) \times 2/3 { f 4 g a }  |
		g 1  |
		r2 \times 2/3 { g 4 a g }  |
%% 30
		g 2 e  |
		r2 \times 2/3 { d 4 e f }  |
		f 2 c  |
		r2 \times 2/3 { g 4 a g }  |
		a 2 e  |
%% 35
		r2 \times 2/3 { f 4 g a }  |
		g 1  |
		r2 \times 2/3 { d 4 c d }  |
		c 1  |
		}
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		¡Só -- lo Dios bas -- ta!
		¡Só -- lo Dios bas -- ta!
		¡Só -- lo Dios bas -- ta! __
		¡A -- le -- lu -- ya!

		¡Só -- lo Dios bas -- ta!
		¡Só -- lo Dios bas -- ta!
		¡Só -- lo Dios bas -- ta!
		¡A -- le -- lu -- ya!

		¡A -- le -- lu -- ya!
	}
>>
