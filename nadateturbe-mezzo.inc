\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

%		\clef "mezzosoprano"
		\clef "treble"

		\transpose c' bes {
			\key c \major
		R1*21  |
		r4 c' e' c'  |
		b 2 b  |
		r4 c' f' c'  |
%% 25
		d' 2 d'  |
		r4 c' e' c'  |
		f' 2 \times 2/3 { f' 4 f' f' }  |
		d' 1  |
		R1  |
%% 30
		r4 c' e' c'  |
		b 2 b  |
		r4 c' f' c'  |
		d' 2 d'  |
		r4 c' e' c'  |
%% 35
		f' 2 \times 2/3 { f' 4 f' f' }  |
		d' 1  |
		r2 \times 2/3 { d' 4 e' f' }  |
		e' 1  |
		}
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		¡Só -- lo Dios bas -- ta!
		¡Só -- lo Dios bas -- ta!
		¡Só -- lo Dios bas -- "ta! ¡A" -- le -- lu -- ya!

		¡Só -- lo Dios bas -- ta!
		¡Só -- lo Dios bas -- ta!
		¡Só -- lo Dios bas -- "ta! ¡A" -- le -- lu -- ya!

		¡A -- le -- lu -- ya!
	}
>>
